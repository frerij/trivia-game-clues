from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg

from .categories import CategoryOut

# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()


class Clue(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool


class Message(BaseModel):
    message: str


@router.get("/api/random-clue/{clue_id}")
def get_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur: 
            cur.execute("""
            SELECT 
                clues.id, 
                clues.answer, 
                clues.question, 
                clues.value, 
                clues.invalid_count, 
                clues.canon, 
                categories.id, 
                categories.title, 
                categories.canon
            FROM clues
            INNER JOIN categories ON (clues.category_id = categories.id)
            WHERE clues.id = %s
            """, [clue_id])

            clue = cur.fetchone()
            if clue is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}
            return {
                    "id": clue[0],
                    "answer": clue[1],
                    "question": clue[2],
                    "value": clue[3],
                    "invalid_count": clue[4],
                    "category": {
                        "id": clue[6],
                        "title": clue[7],
                        "canon:": clue[8]
                    },
                    "canon": clue[5]
            }


@router.get(
    "/api/random-clue",
    response_model = Clue,
    responses={404: {"model": Message}}
)
def get_random_clue(valid: bool = True):
  with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
               f"""
                SELECT cats.id, cats.title, cats.canon,
                        clues.id, clues.question, clues.answer,
                        clues.value, clues.invalid_count,
                        clues.canon, clues.category_id
                FROM categories AS cats
                INNER JOIN clues ON (cats.id = clues.category_id)
                WHERE clues.invalid_count = 0
                ORDER BY RANDOM()
                LIMIT 1
            """
            )
            row = cur.fetchone()
            return ({
                "id": row[3],
                "question": row[4],
                "answer": row[5],
                "value": row[6],
                "invalid_count": row[7],
                "canon": row[8],
                "category": {
                  "id": row[0],
                  "title": row[1],
                  "canon": row[2],
                }
                })
