from fastapi import APIRouter, Response, status
from fastapi.responses import JSONResponse
from pydantic import BaseModel
import psycopg

# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()


class GameIn(BaseModel):
    title: str


class GameOut(BaseModel):
  id: int
  episode_id: int
  aired: str
  canon: bool  


class GameWithClueCount(GameOut):
    total_won: int


class Games(BaseModel):
    page_count: int
    categories: list[GameWithClueCount]


class Message(BaseModel):
    message: str


@router.get(
    "/api/games/{game_id}",
    response_model=GameOut,
    responses={404: {"model": Message}},
)
def get_game(game_id: int):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                SELECT g.id, g.episode_id, g.aired, g.canon,
                SUM(clues.value) AS total_won
                FROM games AS g
                LEFT OUTER JOIN clues
                  ON (clues.game_id = g.id)   
                WHERE g.id = %s
                GROUP BY g.id;               
            """,
                [game_id],
            )
            row = cur.fetchone()
            if row is None:
                return JSONResponse(status_code=404, content={"Message": "Game not found"})
            record = {}
            for i, column in enumerate(cur.description):
                record[column.name] = row[i]
            return record


@router.post(
    "/api/custom-games",
    response_model=GameOut,
    responses={409: {"model": Message}},
)
def create_game(game: GameIn, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            try:
                # Uses the RETURNING clause to get the data
                # just inserted into the database. See
                # https://www.postgresql.org/docs/current/sql-insert.html
                cur.execute(
                    """
                    INSERT INTO categories (title, canon)
                    VALUES (%s, false)
                    RETURNING id, title, canon;
                """,
                    [game.title],
                )
            except psycopg.errors.UniqueViolation:
                # status values at https://github.com/encode/starlette/blob/master/starlette/status.py
                response.status_code = status.HTTP_409_CONFLICT
                return {
                    "message": "Could not create duplicate Game",
                }
            row = cur.fetchone()
            record = {}
            for i, column in enumerate(cur.description):
                record[column.name] = row[i]
            return record
